import { Component, OnInit,ElementRef } from '@angular/core';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  constructor(private elementref:ElementRef) { }

  ngOnInit() {
  }
  displ(){
    document.querySelector('.display-para').classList.remove('hidden-para');
  }

}
