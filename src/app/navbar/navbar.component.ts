import { Component, OnInit,ElementRef,HostListener } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private elementRef:ElementRef ) { }

  ngOnInit() {
  }
  displ(){
    document.querySelector('.ul').classList.remove('list-unstyled');
  }
  hidden(){
    document.querySelector('.ul').classList.add('list-unstyled');

  }
 @HostListener('window:scroll',[])
 onWindowScroll(){
   if(document.body.scrollTop > 0 || document.documentElement.scrollTop > 0){
     document.getElementById('nav3').classList.add('pos');
   }else{
    document.getElementById('nav3').classList.remove('pos');

   }
 }
 

}
