import { Component, OnInit,ElementRef } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private elementRef:ElementRef) { }

  ngOnInit() {}
  customOptions2: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    autoplay: true,

    navText: [`<div class="slider-arrow">
    <div class="arrow next">
      <img src="../../assets/images/icons/next-arrow.svg" alt="next">
    </div>
  </div>`, `<div class="slider-arrow">

  <div class="arrow prev">
    <img src="../../assets/images/icons/prev-arrow.svg" alt="prev">
  </div>
</div>`],
    responsive: {
      0: {
        items: 1
      }
    },
  }

}
